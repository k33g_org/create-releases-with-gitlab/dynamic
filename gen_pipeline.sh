#!/bin/bash
cat > build-library.yml <<- EOM
stages:
  - build library

build job:
  stage: build library
  artifacts:
    paths:
      - my-lib-${LIB_VERSION}.txt
  script: |
    echo "this is my lib ${LIB_VERSION}" > my-lib-${LIB_VERSION}.txt

EOM
